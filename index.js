var valoresGastos = [];
var valoresConsolidados = [];

function retornaMes(mes) {
  const mesesDoAno = [
    { id: 1, nome: "Janeiro" },
    { id: 2, nome: "Fevereiro" },
    { id: 3, nome: "Março" },
    { id: 4, nome: "Abril" },
    { id: 5, nome: "Maio" },
    { id: 6, nome: "Junho" },
    { id: 7, nome: "Julho" },
    { id: 8, nome: "Agosto" },
    { id: 9, nome: "Setembro" },
    { id: 10, nome: "Outubro" },
    { id: 11, nome: "Novembro" },
    { id: 12, nome: "Dezembro" },
  ];
  return mesesDoAno[mes].nome;
}

function orderPayments() {
  const rows = document.getElementById("tb_fatura").getElementsByTagName("tr");

  for (row = 1; row < rows.length; row++) {
    var dados = rows[row].getElementsByTagName("td");

    const valor = dados[1].firstChild.nodeValue;
    const idMes = parseInt(dados[2].firstChild.nodeValue);
    const mes = retornaMes(parseFloat(dados[2].firstChild.nodeValue) - 1);

    valoresGastos.push({
      idMes: idMes,
      mes: mes,
      valor: removeCaracters(valor),
    });
  }

  valoresGastos.sort(compare);
  geraConsolidado();
}

function removeCaracters(valor) {
  valor = valor.replace(".", "").replace(",", ".").replace("R$ ", "");

  return valor;
}

function compare(a, b) {
  if (a.idMes < b.idMes) return -1;
  if (a.idMes > b.idMes) return 1;
  return 0;
}

function geraConsolidado() {
  var mesAnt;

  valoresGastos.forEach((element) => {
    if (mesAnt === element.mes) {
      //Aqui eu tenho que alterar o valor do array.
      valoresConsolidados.forEach((valorConsolidado) => {
        if (mesAnt === valorConsolidado.mes) {
          valorSomado =
            parseFloat(valorConsolidado.valor) + parseFloat(element.valor);
          valorConsolidado.valor = valorSomado;
        }
      });
    } else {
      valoresConsolidados.push({
        mes: element.mes,
        valor: parseFloat(element.valor),
      });
    }
    mesAnt = element.mes;
  });
  imprimeValores();
}

//Aqui não precisa mais mexer.
function imprimeValores() {
  document.getElementById("tb_fatura").innerHTML = "";

  tHead = document.createElement("thead");
  trCabecalho = document.createElement("tr");
  colunasMes = document.createElement("th");
  colunasGasto = document.createElement("th");

  totalGasto = document.createTextNode("Total Gasto");
  mesTotal = document.createTextNode("Mês");

  colunasMes.appendChild(mesTotal);
  colunasGasto.appendChild(totalGasto);

  trCabecalho.appendChild(colunasMes);
  trCabecalho.appendChild(colunasGasto);

  document.getElementById("tb_fatura").appendChild(trCabecalho);

  valoresConsolidados.forEach((element) => {
    tr = document.createElement("tr");
    tdMes = document.createElement("td");
    tdValor = document.createElement("td");

    mes = document.createTextNode(element.mes);
    valor = document.createTextNode(
      "R$" + element.valor.toLocaleString("pt-BR")
    );

    tdMes.appendChild(mes);
    tdValor.appendChild(valor);

    tr.appendChild(tdMes);
    tr.appendChild(tdValor);

    document.getElementById("tb_fatura").appendChild(tr);
  });
}
